function init()
{
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user)
    {
        var menu = document.getElementById('menu');
        // Check user login
        if (user)
        {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e =>
            {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).catch(function(e)
                {
                  create_alert("error",e.message);
                });
            });
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
        }
        else
        {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
      });
}
