window.onload = function () {
        init();
        var locate = document.location.search.replace("?","/");
        //var str_before_username = "<h6 class='border-bottom border-gray pb-2 mb-0'>The Newest</h6><span class='border border-primary'><div class='row'><p><strong class='d-block text-gray-dark'>";
        var first = "<h6 class='border-bottom border-gray pb-2 mb-0'></h6><table class='table'><tbody>"
        var str_before_username = "<tr><td><p>"
        var str_after_content = "</tbody></table>";

        var postsRef = firebase.database().ref('content_list' + locate);
        // List for store posts html
        var total_post = [];
        // Counter for checking history post update complete
        var first_count = 0;
        // Counter for checking when to update new post
        var second_count = 0;

        postsRef.once('value').then(function (snapshot)
        {
          snapshot.forEach(function(childshot){
          var data = childshot.val();
          console.log(data);
          total_post[total_post.length] = str_before_username + "<a class='users' href = 'others.html?" + data.user  + "'>" + data.user + "</a></td><td><a href='article.html?" + locate + "_" + data.user + "/"+ data.topic + "'><strong>" +  data.topic +"</strong></a></td></tr>";
          total_post.reverse();
          first_count += 1;
        });

        document.getElementById('post_list').innerHTML = first + total_post.join('')  + str_after_content;

        postsRef.on('child_added', function (data) {
          second_count += 1;
          if (second_count > first_count) {
            var childData = data.val();
            total_post.reverse();
            total_post[total_post.length] = str_before_username + "<a class = 'users' href = 'others.html?" + data.user  + "'>" + data.user + "</a></td><td><a href='article.html?" + locate + "_" + data.user + "/"+ data.topic + "'><strong>" +  data.topic +"</strong></a></td></tr>";
            total_post.reverse();
            document.getElementById('post_list').innerHTML = first + total_post.join('')  + str_after_content;
          }
        });
      }).catch(function(e)
      {
        create_alert("error",e.message);
      });
    }

    /*
    <div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>The Newest
      <h6 class='border-bottom border-gray pb-2 mb-0'>
        <div class='media text-muted pt-3'>
          <img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>
        </h6>
      <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>*/
